﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPaddle : MonoBehaviour {
    public Transform target;
    Vector3 move = Vector3.zero;
    Vector3 direction = Vector3.zero;
    float speed = 10.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float d = target.position.y - transform.position.y;
        if (d > 0)
        {
            move.y = speed * Mathf.Min(d, 1.0f);
        }
        else
        {
            move.y = -(speed * Mathf.Min(-d, 1.0f));
        }
        transform.position += move * Time.deltaTime;
    }
}
