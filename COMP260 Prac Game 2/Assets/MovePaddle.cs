﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;

	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {

        //moves the paddle by velocity
        Vector3 direction = Vector3.zero;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");
        rigidbody.velocity = direction * speed;
	}

	public float speed = 10f;

	void FixedUpdate () {

        //moves the paddle by addForce
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVectical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, moveVectical);

		//rigidbody.velocity = speed;
		rigidbody.AddForce (movement * speed);

//		Vector3 pos = GetMousePosition();
//		Vector3 dir = pos - rigidbody.position;
//		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
//		float move = speed * Time.fixedDeltaTime;
//		float distToTarget = dir.magnitude;

//		if (move > distToTarget) {
			// scale the velocity down appropriately
//			vel = vel * distToTarget / move;
//		}

//		rigidbody.velocity = vel;
	}

}
